#include <iostream>
#include <string>
#include <fstream>
#include "extractfield.h"

using namespace std;

void extractfield(int field, istream & in, ostream & out)
{
  int i = 0; //number of colons
  string line;
  while (getline(in, line))
  {
    for (int j = 0;line[j];j++)
    {
      if (line[j] == ':')
        i++;
      else
       if (i == field - 1)
       {
        while ((line[j] != ':') && (line[j]))
        {
          out << line[j];
          j++;
        }
        i = 0;
        out << endl;
        break;
      }
    }
  }
  return;
}

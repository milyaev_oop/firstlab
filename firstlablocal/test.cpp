#include <gtest/gtest.h>
#include "filter.h"
#include "extractfield.h"

using namespace std;
TEST(filter,test_simple)
{
  stringstream in("one:two:three\nfoo:bar:baz\n");
  string exp_out="one:two:three\n";
  stringstream out;
  filter("one",in,out);
  ASSERT_EQ(out.str(),exp_out);
}
TEST(filter,test_simple1)
{
  stringstream in("one:two:three\nfoo:bar:baz\n");
  string exp_out="";
  stringstream out;
  filter("fou",in,out);
  ASSERT_EQ(out.str(),exp_out);
}
TEST(filter,test_simple2)
{
  stringstream in("one:two:foo\nfoo:bar:baz\n");
  string exp_out="one:two:foo\nfoo:bar:baz\n";
  stringstream out;
  filter("foo",in,out);
  ASSERT_EQ(out.str(),exp_out);
}
TEST(extractfield,test_simple3)
{
  stringstream in("one:two:foo\nfoo:bar:baz\n");
  string exp_out="foo\nbaz\n";
  stringstream out;
  extractfield(3,in,out);
  ASSERT_EQ(out.str(),exp_out);
}
TEST(extractfield,test_simple4)
{
  stringstream in("one:two:foo\nfoo:bar:baz\n");
  string exp_out="two\nbar\n";
  stringstream out;
  extractfield(2,in,out);
  ASSERT_EQ(out.str(),exp_out);
}
TEST(extractfield,test_simple5)
{
  stringstream in("one:two:foo\nfoo:bar:baz\n");
  string exp_out="one\nfoo\n";
  stringstream out;
  extractfield(1,in,out);
  ASSERT_EQ(out.str(),exp_out);
}

#include <iostream>
#include <string>
#include <fstream>
#include "filter.h"

using namespace std;

void filter(string pattern, istream & in, ostream & out)
{
  string line;
  while (getline(in, line))
    if (line.find(pattern) != string::npos)
      out << line << endl;
  return;
}

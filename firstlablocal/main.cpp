#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include "filter.h"
#include "extractfield.h"
using namespace std;

int main(int argc, char **argv)
{
  if (argc<3)
    cout << "Not enough arguments\n";
  if (argc == 3)
  {
    stringstream buffer;
    filter(argv[1],cin,buffer);
    extractfield(atoi(argv[2]),buffer,cout);
  }
  if (argc == 4)
  {
    ifstream fin;
    fin.open(argv[3]);
    stringstream buffer;
    filter(argv[1],fin,buffer);
    extractfield(atoi(argv[2]),buffer,cout);
  }
  if (argc == 5)
  {
    ifstream fin;
    ofstream fout;
    fin.open(argv[3]);
    fout.open(argv[4]);
    stringstream buffer;
    filter(argv[1],fin,buffer);
    extractfield(atoi(argv[2]),buffer,fout);
  }
  return 0;
}
